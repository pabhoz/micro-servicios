-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema medimos_s_a
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `medimos_s_a` ;

-- -----------------------------------------------------
-- Schema medimos_s_a
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `medimos_s_a` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `medimos_s_a` ;

-- -----------------------------------------------------
-- Table `medimos_s_a`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medimos_s_a`.`User` ;

CREATE TABLE IF NOT EXISTS `medimos_s_a`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `username` VARCHAR(45) NOT NULL COMMENT '',
  `password` VARCHAR(128) NOT NULL COMMENT '',
  `email` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medimos_s_a`.`Encuesta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medimos_s_a`.`Encuesta` ;

CREATE TABLE IF NOT EXISTS `medimos_s_a`.`Encuesta` (
  `id` INT NOT NULL COMMENT '',
  `titulo` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medimos_s_a`.`Pregunta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medimos_s_a`.`Pregunta` ;

CREATE TABLE IF NOT EXISTS `medimos_s_a`.`Pregunta` (
  `id` INT NOT NULL COMMENT '',
  `pregunta` VARCHAR(45) NOT NULL COMMENT '',
  `id_encuesta` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_preguntas_encuestas1_idx` (`id_encuesta` ASC)  COMMENT '',
  CONSTRAINT `fk_preguntas_encuestas1`
    FOREIGN KEY (`id_encuesta`)
    REFERENCES `medimos_s_a`.`Encuesta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `medimos_s_a`.`Respuesta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `medimos_s_a`.`Respuesta` ;

CREATE TABLE IF NOT EXISTS `medimos_s_a`.`Respuesta` (
  `id_usuario` INT NOT NULL COMMENT '',
  `id_pregunta` INT NOT NULL COMMENT '',
  `respuesta` VARCHAR(1) NOT NULL COMMENT '',
  PRIMARY KEY (`id_usuario`, `id_pregunta`)  COMMENT '',
  INDEX `fk_respuestas_preguntas1_idx` (`id_pregunta` ASC)  COMMENT '',
  CONSTRAINT `fk_respuestas_users`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `medimos_s_a`.`User` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_respuestas_preguntas1`
    FOREIGN KEY (`id_pregunta`)
    REFERENCES `medimos_s_a`.`Pregunta` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
