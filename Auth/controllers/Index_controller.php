<?php

class Index_controller extends Controller {

    function __construct() {
        parent::__construct();
    }
        
        public function getIndex($id = null){

            if(!is_null($id)){
             Penelope::printJSON(User::getById($id)->toArray()); 
            }else{
             Penelope::printJSON(User::getAll());   
            }

        }
        
        public function postIndex(){
            $keys = ["username","password","email"];
            $this->validateKeys($keys, $_POST);
            $password = new CUrlClient(_ENCRYPT_SERVICE."?data=".$_POST["password"]);
            $password = $password->execute()["encrypted_data"];
                 
            $usr = new User("",$_POST["username"],$password,$_POST["email"]);
            $usr->create();
            $response = [
               "status"=>200,
                "msg"=>"User created",
                "user"=>  Penelope::arrayToJSON($usr->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function putIndex(){
            $_PUT = $this->_PUT;
            $usr = User::getById($_PUT["id"]);

            foreach ($_PUT as $key => $value) {
                if($key == "password"){
                    $password = new CUrlClient(_ENCRYPT_SERVICE."?data=".$_POST["password"]);
                    $password = $password->execute()["encrypted_data"];
                    $usr->{"set".ucfirst($key)}($password);
                }else{
                    $usr->{"set".ucfirst($key)}($value);
                }
            }
            $usr->update();
            
            $response = [
               "status"=>200,
                "msg"=>"User updated",
                "user"=>  Penelope::arrayToJSON($usr->toArray())
            ];
            Penelope::printJSON($response);
        }
        
        public function deleteIndex(){
            $_DELETE = $this->_DELETE;
            $usr = User::getById($_DELETE["id"]);
            if($usr->getId() != NULL){
                $result = $usr->delete();
                if($result["error"]==1){
                    $response = [
                        "status"=>200,
                         "msg"=>"Imposible to delete",
                         "error"=>$result["message"][2]
                     ];
                }else{
                    $response = [
                        "status"=>200,
                         "msg"=>"User created",
                         "user"=>  Penelope::arrayToJSON($usr->toArray())
                     ];
                }
            }else{
                $response = [
                        "status"=>200,
                         "msg"=>"User not found"
                     ];
            }
            Penelope::printJSON($response);
            
        }
}

